# Heart Rate Aggregator

This project is a Node.js application for aggregating heart rate data. The application is built with Express and Sequelize, and uses PostgreSQL as the database.

### Prerequisites

- Node.js (version 18 or higher)
- npm (version 10 or higher)
- PostgreSQL

### Installation

1. **Clone the repository:**

   ```sh
   git clone https://gitlab.com/contact2satish.patna/heart-rates.git
   cd heart-rate
   ```

2. **Install the dependencies:**

   ```sh
   npm install
   ```

3. **Set up the database:**

   Ensure you have PostgreSQL running and create a database for the project. Update the database configuration in `src/database.ts` with your database credentials using .env variable mentioned in .env.example.

   ```Bash
   const sequelize = new Sequelize(String(process.env.DATABASE), String(process.env.USER_NAME), process.env.PASSWORD, {
    host: process.env.HOST,
    dialect: 'postgres',
    logging: false
   })
   ```

### Running the Application

1. **Start the server:**

   ```sh
   npm start
   ```

2. **The server should now be running at `http://localhost:3000`.**

### Usage

You can test the API using Postman. Below is an example of how to send a request to the `/heartRateAggr` endpoint with a JSON file.

- **Endpoint:** `POST http://localhost:3000/heartRateAggr`
- **Body:**
  - **Type:** `form-data`
  - **Key:** `clinical_metric`
  - **Value:** \_Upload your JSON file (e.g., clinical_metrics.json)

```sh
   curl --location 'http://localhost:3000/heartRateAggr' \
--form 'clinical_matric=@"/Users/satishkumar/Downloads/clinical_metrics.json"'
```
