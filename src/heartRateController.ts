import { Request, Response } from 'express';
import fs from 'fs';
import moment from 'moment'
import HeartRate from './modals/heart-rate';

interface Measurement {
    on_date: string;
    measurement: string;
}

interface ClinicalData {
    HEART_RATE: {
        uom: string;
        data: Measurement[];
    };
}

const calculateHeartRate = async (clinicalData: ClinicalData) => {
    try {
        const heartRateData = clinicalData.HEART_RATE.data;

        const groupedData: { [key: string]: { low: number, high: number } } = {};

        heartRateData.forEach(item => {
            const date = moment(item.on_date);
            const interval = Math.floor(date.minutes() / 15) * 15;
            const intervalKey = date.minutes(interval).seconds(0).milliseconds(0).toISOString();

            if (!groupedData[intervalKey]) {
                groupedData[intervalKey] = { low: Number(item.measurement), high: Number(item.measurement) };
            } else {
                groupedData[intervalKey].low = Math.min(groupedData[intervalKey].low, Number(item.measurement));
                groupedData[intervalKey].high = Math.max(groupedData[intervalKey].high, Number(item.measurement));
            }
        });

        const processedData = Object.keys(groupedData).map(key => {

            let result = {
                from_date: key,
                to_date: moment(key).add(15, 'm').toISOString(),
                measurement: groupedData[key]
            }
            return result
        }
        );
        await HeartRate.bulkCreate(processedData)

        return processedData
    } catch (error) {
        throw error
    }

}

const heartRateController = async (req: Request | any, res: Response) => {
    try {
        const { files } = req.body
        const fileName = Object.keys(files)[0];
        let oldPath: string = files[fileName][0].filepath;

        fs.readFile(oldPath, 'utf8', async (err, data) => {
            if (err) {
                return res.status(500).json({ error: err.message });
            }

            const result = await calculateHeartRate(JSON.parse(data).clinical_data)

            fs.unlink(oldPath, (err) => {
                if (err) {
                    console.log('.... Error while deleting  temp file...', err)
                } else {
                    console.log('......temp file deleted .....',)
                }
            })

            return res.status(200).json(result)
        });
    } catch (error) {

        return res.status(500).json(error);
    }




};

export default heartRateController;
