import { DataTypes, Model } from 'sequelize';
import sequelize from '../database';

class HeartRate extends Model {
    public id!: string;
    public from_date!: string;
    public to_date!: string;
    public measurement!: any;
}

HeartRate.init({
    id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false
    },
    from_date: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    to_date: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    measurement: {
        type: DataTypes.JSON,
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'heart_rate',
});

export default HeartRate;
