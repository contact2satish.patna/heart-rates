import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import bodyParser from 'body-parser';
import heartRateController from './heartRateController';
import { parseFiles } from './middleware/fileHandler';
import sequelize from './database';



const app = express();
const port = 3000;

app.use(bodyParser.json());

sequelize.authenticate().then(() => {
    console.log('Connection has been established successfully.');

    sequelize.sync({ alter: true })
        .then(() => {
            console.log('Database synchronized');
        })
        .catch((error: any) => {
            console.error('Error synchronizing database:', error);
        });

}).catch(error => {
    console.error('Unable to connect to the database:', error);

})


app.post('/heartRateAggr', parseFiles, heartRateController);

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
