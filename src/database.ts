import { Sequelize } from 'sequelize';

const sequelize = new Sequelize(String(process.env.DATABASE), String(process.env.USER_NAME), process.env.PASSWORD, {
    host: process.env.HOST,
    dialect: 'postgres',
    logging: false
});

export default sequelize;
