import { NextFunction, Request, Response } from 'express';
import formidable from 'formidable';
import path from 'path';
import fs from 'fs';

export const parseFiles = (req: Request, res: Response, next: NextFunction) => {

    const form = formidable({});

    form.parse(req, (err, fields, files) => {
        if (err) {
            return res.status(500).json({ error: 'Internal Server Error' });
        }
        else {
            req.body = { fields, files: files }
            next()
        }
    })
}